const startGameBtn = document.getElementById('start-game-btn');

const ROCK = 'ROCK';
const PAPER = 'PAPER';
const SCISSORS = 'SCISSORS';
const DEFAULT_USER_CHOICE = ROCK;
const RESULT_DRAW = 'DRAW';
const RESULT_PLAYER_WINS = 'PLAYER_WINS';
const RESULT_COMPUTER_WINS = 'COMPUTER_WINS';

let gameIsRunning = false;

const getPlayerChoice = () => {
  const selection = prompt(`${ROCK}, ${PAPER} or ${SCISSORS}`, '').toUpperCase();
  if (selection !== ROCK
        && selection !== PAPER
        && selection !== SCISSORS
  ) {
    alert(`Invalid choice! We chose ${DEFAULT_USER_CHOICE} for you`);
    return;
  }
  return selection;
};

const getComputerChoice = () => {
  let randomValue = Math.random();
  if (randomValue < 0.34) {
    return ROCK;
  } if (randomValue < 0.67) {
    return PAPER;
  }
  return SCISSORS;
};

const getWinner = (cChoice, pChoice = DEFAULT_USER_CHOICE) => {
  if (cChoice === pChoice) {
    return RESULT_DRAW;
  } if (
    (cChoice === ROCK && pChoice === PAPER)
      || (cChoice === PAPER && pChoice === SCISSORS)
    || (cChoice === SCISSORS && pChoice === ROCK)
  ) {
    return RESULT_PLAYER_WINS;
  }
  return RESULT_COMPUTER_WINS;
};

startGameBtn.addEventListener('click', () => {
  if (gameIsRunning) {
    return;
  }
  gameIsRunning = true;
  console.log('Game is starting...');
  const playerChoice = getPlayerChoice();
  const computerChoice = getComputerChoice();
  let winner;
  if (playerChoice) {
    winner = getWinner(computerChoice, playerChoice);
  } else {
    winner = getWinner(computerChoice, playerChoice);
  }
  let message;
  if (winner === RESULT_DRAW) {
    message = `You picked ${playerChoice || DEFAULT_USER_CHOICE}, computer picked ${computerChoice}, therefore you had a draw`;
  } else if (winner === RESULT_PLAYER_WINS) {
    message = `You picked ${playerChoice || DEFAULT_USER_CHOICE}, computer picked ${computerChoice}, therefore you had won`;
  } else if (winner === RESULT_COMPUTER_WINS) {
    message = `You picked ${playerChoice || DEFAULT_USER_CHOICE}, computer picked ${computerChoice}, therefore computer had won`;
  }
  alert(message);
  gameIsRunning = false;
});

// not related to game

// const sumUp = (resultHandler, ...numbers) => {
//   const validateNumber = (number) => (isNaN(number) ? 0 : number);
//   let sum = 0;
//   for (const num of numbers) {
//     sum += validateNumber(num);
//   }
//   resultHandler(sum);
// };
//
// const subtractUp = (resultHandler, ...numbers) => {
//   let sum = 0;
//   for (const num of numbers) {
//     sum -= num;
//   }
//   return resultHandler(sum);
// };
//
// const showResult = (messageText, result) => {
//   alert(`${messageText} ${result}`);
// };
//
// (sumUp(showResult.bind(this, 'The result after adding all numbers is:'), 1, 5, 'fsda', -3, 6, 10));
// (sumUp(showResult.bind(this, 'The result after adding all numbers is:'), 1, 1, 5, 'fsda', -3, 6, 10));
// (subtractUp(showResult.bind(this, 'The result after subtracting all numbers is:'), 1, 2, 3, 15));
